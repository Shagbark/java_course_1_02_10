package org.levelup.lesson4;

import org.levelup.lesson4.inheritance.*;

public class Test {

    public static void main(String[] args) {
        Animal animal = AnimalFactory.createAnimal(1);
        animal.eat();



        Animal dogAnimal = AnimalFactory.createAnimal(2);
        dogAnimal.eat();
    }

}
