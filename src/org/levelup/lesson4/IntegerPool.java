package org.levelup.lesson4;

@SuppressWarnings("ALL")
public class IntegerPool {

    public static void main(String[] args) {
        Integer i1 = 127;   // 4867
        Integer i2 = 127;   // 4867
        Integer i3 = 129;   // 58
        Integer i4 = 129;   // 96

        System.out.println(i1 == i2);
        System.out.println(i4 == i3);
    }

}
