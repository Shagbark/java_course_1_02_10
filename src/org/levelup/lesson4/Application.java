package org.levelup.lesson4;

import java.util.*;

public class Application {

    public static void main(String[] args) {
        Product first = new Product(null);
        Product second = new Product("Second");
        Product third = new Product("First");

        System.out.println(first == third); // -> false
        if (first.hashCode() == third.hashCode()) {
            System.out.println(first.equals(third)); // -> true
        }

        Object[] objects = new Object[4];
        objects[0] = first;
        objects[1] = second;
        objects[2] = third;

        for (int i = 0; i < objects.length; i++) {
            if (objects[i].equals(first)) {
                System.out.println("index: " + i);
            }
        }

        int intValue;
        Integer integerValue;
        char c;
        Character character;

        double doubleValue;
        Double dValue;



    }

}
