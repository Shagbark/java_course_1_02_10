package org.levelup.lesson4;

import java.util.*;

@SuppressWarnings("ALL")
public class Product {

    String name;

    public Product(String name) {
        this.name = name;
    }

//    @Override
//    public boolean equals(Object obj) {
//        //
//        if (this == obj) return true;
//        // 2 способа проверить тип класса
//        // instanceof
//        // <obj> instanceof Object
//        // if (!(obj instanceof Product)) return false;
//        if (obj == null || obj.getClass() != this.getClass()) return false;
//
//        Product other = (Product) obj;
//        // return name != null && name.equals(other.name);
//        return Objects.equals(name, other.name);
//    }
//
//    // checksum md5 532tvrmvuavasjkwhgiur
//    @Override
//    public int hashCode() {
//        // Objects.hash(1, 2, 4)
//        return Objects.hash(name);
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(name, product.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

}
