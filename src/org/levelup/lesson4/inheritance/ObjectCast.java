package org.levelup.lesson4.inheritance;

@SuppressWarnings("ALL")
// ClassCastException
public class ObjectCast {

    // child as is parent
    public static void main(String[] args) {
        //
        Cat cat = new Cat();

        Animal[] animals = new Animal[4];
        animals[0] = new Animal("1");
        animals[1] = new Cat("2");
        animals[2] = new Dog("3");
        animals[3] = new Dog("4");

        feedAnimals(animals);
//        Animal animal = createAnimal();  // Animal animal =(Animal) cat;
//        animal.eat();
        Animal animal = cat;
//        Object object = cat;
        // Object object2 = animal;

//        Cat myCat = (Cat) object;

//        Animal newAnimal = new Animal();
//        Cat catFromAnimal = (Cat) newAnimal;

        // Object newObject = new Object();
        // Cat catFromObjec = (Cat) newObject;

//        printAnimalsNames(cat);
//        printAnimalsNames(new Animal());
//        printAnimalsNames(new Dog());
    }

//    static void printAnimalsNames(Cat cat) {
//
//    }

    static Animal createAnimal() {
        return new Cat();
    }

    static void feedAnimals(Animal[] animals) {
        for (int i = 0; i < animals.length; i++) {
            animals[i].eat();
//            System.out.println(animals[i].eat(););
        }
    }
//
//    static void printAnimalsNames(Dog dog) {
//
//    }

}
