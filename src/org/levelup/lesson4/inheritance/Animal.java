package org.levelup.lesson4.inheritance;

// superclass, base class (базовый класс)
@SuppressWarnings("ALL")
public class Animal {

    double weight;
    public String name;
    int age;

    Animal(String name) {
        this.name = name;
    }

    Animal() {
        System.out.println("Вызван конструктор Animal...");
    }

    Animal(String name, double weight, int age) {
        System.out.println("Вызван конструктор Animal...");
    }

    public void eat() {
        System.out.println("Животное кушает...");
    }

}
