package org.levelup.lesson4.inheritance;

// подкласс, subclass
class Cat extends Animal {

    Cat(String name) {
        super(name);
    }

    Cat() {
        // super("", 15,55 );
        //this(");
        System.out.println("Вызван конструктор Cat...");
    }

    void meow() {
        System.out.println("Мяю...");
    }

    // Переопределение метода (Overriding)
    public void eat() {
        // super.eat();
        System.out.println("Кот кушает и бежит спать...");
    }

}
