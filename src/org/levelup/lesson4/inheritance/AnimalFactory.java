package org.levelup.lesson4.inheritance;

public class AnimalFactory {

    public static Animal createAnimal(int value) {
        if (value == 1) {
            return new Cat();
        }
        return new Dog("");
    }

}
