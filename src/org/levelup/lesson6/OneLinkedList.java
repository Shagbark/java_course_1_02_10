package org.levelup.lesson6;

public class OneLinkedList {

    // Head - начало списка
    // Tail - конец списка
    private ListElement head;

    public void addLast(int value) {
        ListElement element = new ListElement(value);
        if (head == null) {
            // Список пустой
            head = element;
        } else {
            ListElement current = head;
            while (current.getNext() != null) {
                current = current.getNext();
            }
            current.setNext(element);
//            while (head.getNext() != null) {
//                head = head.getNext();
//            }
        }
    }

    public int get(int index) {
        if (index < 0) {
            // String.valueOf(index); -> index -> "0"
            throw new IndexOutOfBoundsException(index);
        }
        return 0;
    }

}
