package org.levelup.lesson6;

// Список на основе массива (динамический массив)
public class ListBasedOnArray {

    // Массив, в котором хранятся данные
    private int[] elementData;
    // Количество элементов в списке (в массиве)
    private int size;

    public ListBasedOnArray(int initialCapacity) {
        this.elementData = new int[initialCapacity];
    }

    // Добавление элемента в конец
    public void addLast(int value) {
        if (elementData.length == size) {
            // Увеличиваем размер массива
            int[] oldArray = elementData;
            elementData = new int[(int)(elementData.length * 1.5)];
            System.arraycopy(oldArray, 0, elementData, 0, oldArray.length);
        }

        // elementData[size] = value;
        // size = size + 1; (size++);
        elementData[size++] = value;
    }

    // Удаление элемента по индексу



}
