package org.levelup.lesson6;

public class ListElement<TYPE> {

    private ListElement next;
    private TYPE value;

    public ListElement(TYPE value) {
        this.value = value;
    }

    public ListElement getNext() {
        return next;
    }

    public void setNext(ListElement next) {
        this.next = next;
    }

    public TYPE getValue() {
        return value;
    }

    public void setValue(TYPE value) {
        this.value = value;
    }
}
