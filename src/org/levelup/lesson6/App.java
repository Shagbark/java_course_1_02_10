package org.levelup.lesson6;

import org.levelup.lesson5.Product;

import java.util.ArrayList;
import java.util.List;

public class App {

    public static void main(String[] args) {
        ListBasedOnArray list = new ListBasedOnArray(5);
        list.addLast(4);
        list.addLast(6);
        list.addLast(7);
        list.addLast(2);
        list.addLast(15);
        list.addLast(16);
        list.addLast(87);

        OneLinkedList linkedList = new OneLinkedList();
        linkedList.addLast(10);
        linkedList.addLast(14);
        linkedList.addLast(16);
        linkedList.addLast(21);
        linkedList.addLast(34);

        ListElement<String> listElement = new ListElement<>("");
//        listElement.
        ListElement<Product> productListElement = new ListElement<>(null);

        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.remove(10);


        ArrayList<String> strings = new ArrayList<>();

    }

}
