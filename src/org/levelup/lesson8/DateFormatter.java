package org.levelup.lesson8;

// java.util.Date
// java.sql.Date
// с 8 Java: LocalDate, LocalDateTime\

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

// ms, 1.01.1970
public class DateFormatter {

    // arsdasdfaw
    public void parse(String value) throws ParseException {
        // string -> date
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

        // try-catch-finally
        // try-catch
        // try-finally
        // try-catch-catch-catch
        // try {
        System.out.println("Try to parse value...");
        sdf.parse(value);
        System.out.println("Parse successful...");
        //    throw new RuntimeException();
        //} catch (ParseException exc) {
        //    exc.printStackTrace();
        //} finally {
        //    System.out.println("Finally...");
        //}
    }

}
