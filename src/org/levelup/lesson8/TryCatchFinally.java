package org.levelup.lesson8;

import java.util.Random;

@SuppressWarnings("ALL")
public class TryCatchFinally {

    static int generateNumber() {
        try {
            System.exit(0);
            //Random random = new Random();
            //if (random.nextInt(10) > 3) {
                return 1;
            //}
            //throw new RuntimeException();
        } catch (RuntimeException exc) {
            return 2;
        } finally {
            System.out.println("finally");
//            return 3;
        }
    }

    public static void main(String[] args) {
        System.out.println(generateNumber());
    }

}
