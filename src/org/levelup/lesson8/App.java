package org.levelup.lesson8;

import java.text.ParseException;

public class App {

    public static void main(String[] args) {
        DateFormatter formatter = new DateFormatter();

        try {
            formatter.parse("25.10.2019 20:18:15");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println("After parse");

        Integer s;
        try {
            s = sum(null, 56);
        } catch (IllegalArgumentException exc) {
            System.out.println("Invalid arguments");
        }

        /// ....
    }

    static int sum(Integer x, Integer y) {
        if (x == null || y == null) {
            // IllegalArgumentException exc = new IllegalArgumentException();
            // throw exc;
            throw new IllegalArgumentException();
        }
        return x + y;
    }

}
