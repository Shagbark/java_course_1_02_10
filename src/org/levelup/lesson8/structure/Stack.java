package org.levelup.lesson8.structure;

//@SuppressWarnings("ALL")
public class Stack<T> {

    private Object[] elements;
    private int size;

    public Stack(int stackSize) {
        this.elements = new Object[stackSize];
    }

    public void push(T value) {
        if (size == elements.length) {
            // Стек заполнен
            throw new StackOverflowException();
        }
    }

    public T pop() throws EmptyStackException {
        if (size == 0) {
            throw new EmptyStackException();
        }
        // size - 1
        return (T) elements[size--];
    }

}
