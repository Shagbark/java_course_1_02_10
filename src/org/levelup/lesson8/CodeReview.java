package org.levelup.lesson8;

public class CodeReview {

    public Integer multiply(Integer x, Integer y) {
        return y == 0 ? 0 : multiply(x, y - 1) + x;
        // if (y == 0) {
        //      return 0;
        // } else {
        //      return multiply(x, y - 1) + x;
        // }
    }

}
