package org.levelup.lesson9;

public class OuterClass {

    // Внутренние классы:
    // Inner class
    // Nested class
    // LocalClass
    // Анонимный внутренний класс

    public int publicVar;
    private int privateVar;
    private static int staticVar;

    // public, private, protected, default
    class InnerClass {
        public void method() {
            publicVar = 4;
            privateVar = 10;
            staticVar = 40;
        }
    }

    static class NestedClass {
        public void method() {
            staticVar = 15;
        }
    }

}
