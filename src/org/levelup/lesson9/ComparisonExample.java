package org.levelup.lesson9;

import java.util.*;
import java.util.stream.Collectors;

public class ComparisonExample {

    static class Laptop {
        double cpu;
        String vendorName;
        Laptop(double cpu, String vendorName) {
            this.cpu = cpu;
            this.vendorName = vendorName;
        }

        public static double getCpu(Laptop laptop) {
            return laptop.cpu;
        }

        @Override
        public String toString() {
            return "Laptop{" + "cpu=" + cpu + ", vendorName='" + vendorName + '\'' + '}';
        }
    }

    public static void main(String[] args) {
        List<Laptop> laptops = new ArrayList<>();
        laptops.add(new Laptop(2.4, "Lenovo"));
        laptops.add(new Laptop(2.6, "Acer"));
        laptops.add(new Laptop(3.0, "Asus"));
        laptops.add(new Laptop(3.6, "Asus"));
        laptops.add(new Laptop(1.4, "Dell"));
        laptops.add(new Laptop(2.1, "Macbook"));
        laptops.add(new Laptop(3.0, "HP"));

//        for (Laptop laptop : laptops) {
//            System.out.println(laptop.toString());
//        }
        laptops.forEach(System.out::println);
        laptops.forEach(laptop -> System.out.println(laptop));

        double averageCpu = laptops.stream()
                .mapToDouble(Laptop::getCpu)
//                .mapToDouble(laptop -> Laptop.getCpu(laptop))
                .average()
                .orElse(0);

        laptops = laptops.stream()
                .filter(laptop -> laptop.cpu > 3.0)
                .map(laptop -> {
                    laptop.cpu = laptop.cpu + 0.1;
                    return laptop;
                })
                .collect(Collectors.toList());


        Map<String, Integer> maps = new HashMap<>();
        maps.forEach((key, value) -> System.out.println(key + " " + value));

//
//        Collections.sort(laptops, (o1, o2) -> {
//            ///
//            return o1.vendorName.compareTo(o2.vendorName);
//        });
//
//        Collections.sort(laptops, new Comparator<Laptop>() {
//            @Override
//            public int compare(Laptop o1, Laptop o2) {
//                return o1.vendorName.compareTo(o2.vendorName);
//            }
//        });
    }

}
