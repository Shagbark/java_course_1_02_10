package org.levelup.lesson9;

import java.util.Collection;
import java.util.Iterator;

public class App {

    public static void main(String[] args) {
//        OuterClass outerClass = new OuterClass();
//        OuterClass.InnerClass innerClass = outerClass.new InnerClass();

        OuterClass.InnerClass innerClass = new OuterClass().new InnerClass();
        OuterClass.NestedClass nestedClass = new OuterClass.NestedClass();

        class LocalClass {
            int[] variables;
            String[] operations;
        }
        LocalClass localClass = new LocalClass();

        Collection<String> strings = new Collection<String>() {

            @Override
            public int size() {
                return 0;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public boolean contains(Object o) {
                return false;
            }

            @Override
            public Iterator<String> iterator() {
                return null;
            }

            @Override
            public Object[] toArray() {
                return new Object[0];
            }

            @Override
            public <T> T[] toArray(T[] a) {
                return null;
            }

            @Override
            public boolean add(String s) {
                return false;
            }

            @Override
            public boolean remove(Object o) {
                return false;
            }

            @Override
            public boolean containsAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean addAll(Collection<? extends String> c) {
                return false;
            }

            @Override
            public boolean removeAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean retainAll(Collection<?> c) {
                return false;
            }

            @Override
            public void clear() {

            }
        };
        Iterable<String> iterable = new Iterable<String>() {
            @Override
            public Iterator<String> iterator() {
                // innerClass = new OuterClass().new InnerClass();
                return null;
            }
        };
    }

}
