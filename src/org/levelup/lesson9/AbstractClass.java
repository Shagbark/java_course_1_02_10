package org.levelup.lesson9;

public abstract class AbstractClass {

    public abstract void method();

    public static AbstractClass getInstance() {
        return new DefaultClass();
    }

    private static class DefaultClass extends AbstractClass {
        @Override
        public void method() {

        }
    }

}
