package org.levelup.lesson9;

import java.io.*;

@SuppressWarnings("ALL")
public class FileExample {

    public static void main(String[] args) {
        File testFile = new File("test.txt");
        System.out.println(testFile == null);
        // Проверка, существует ли файл на ФС или нет
        System.out.println(testFile.exists());

        //
        boolean createFileResult = false;
        try {
            createFileResult = testFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Create new file result: " + createFileResult);

        File srcDir = new File("src/");
        // srcDir.mkdir();
        System.out.println(srcDir.exists());
        // src/lesson10/package1
        System.out.println(new File("src/lesson10/package1").mkdirs());


        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(testFile));
//            String line = reader.readLine();
//            while (line != null) {
//                line = reader.readLine();
//            }
            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
        } catch (FileNotFoundException fnf) {
            System.out.println("Файл не найден...");
        } catch (IOException ioexc) {
            System.out.println("Ошибка чтения файла...");
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


        // try-with-resources
        try (BufferedReader bufReader = new BufferedReader(new FileReader(testFile))) {
            // read lines
        } catch (IOException ioexc) {
            System.out.println("Ошибка чтения файла...");
        }

        // cin >> variable;
        // Чтение строк с консоли
        try (BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in))) {

        } catch (IOException exc) {

        }

    }

}
