package org.levelup.lesson10;

@SuppressWarnings("ALL")
class Superclass {
    void method() {}

    class Inner extends Superclass {
        void method() {}
    }

}

@SuppressWarnings("ALL")
public class Subclass extends Superclass {

    void method() {}

    public static void main(String[] args) {
        Subclass subclass = new Subclass();
        subclass.method();
        // a_load1
        // invokevirtual #
    }

}
