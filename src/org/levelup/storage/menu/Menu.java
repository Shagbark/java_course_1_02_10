package org.levelup.storage.menu;

import org.levelup.storage.domain.Product;
import org.levelup.storage.io.ProductStorageService;
import org.levelup.storage.service.ProductService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class Menu {

    private ProductService productService;

    public void startMenu(ProductService productService, ProductStorageService productStorageService) {
        this.productService = productService;

        try (BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in))) {
            System.out.println("1. Вывести список товаров");
            System.out.println("2. Добавить товар");
            System.out.println("3. Удалить товар");
            System.out.println("0. Выход");

            int commandNumber;
            while ((commandNumber = readCommandNumber(consoleReader)) != 0) {
                // if (command == 1) {} else if (command == 2) ...
                // switch-case
                switch (commandNumber) {
                    case 1:
                        List<Product> products = productService.getProducts();
                        products.forEach(System.out::println);
                        break;
                    case 2:
                        Product product = enterNewProduct(consoleReader);
                        productService.addProduct(product);

                        productStorageService.saveProducts(productService.getProducts());
                        break;
                    case 3:
                        break;
                    default:
                        System.out.println("Вы ввели неправильный номер команды");
                        break;
                }
            }


        } catch (IOException exc) {
            throw new RuntimeException("Ошибка чтения с консоли");
        }

    }

    private int readCommandNumber(BufferedReader consoleReader) throws IOException {
        String command = consoleReader.readLine();
        try {
            return Integer.parseInt(command);
        } catch (NumberFormatException exc) {
            System.out.println("Вы ввели неправильное число. Попробуйте снова");
            return -1;
        }
    }

    private Product enterNewProduct(BufferedReader consoleReader) throws IOException {
        System.out.println("Введите название");
        String name = consoleReader.readLine();
        System.out.println("Введите диагональ");
        double diagonal = Double.parseDouble(consoleReader.readLine());
        System.out.println("Введите процессор");
        String cpu = consoleReader.readLine();
        System.out.println("Введите количество оперативной памяти");
        int ram = Integer.parseInt(consoleReader.readLine());
        System.out.println("Введите вес товара");
        double weight = Double.parseDouble(consoleReader.readLine());

        return new Product(name, diagonal, cpu, ram, weight);
    }

}
