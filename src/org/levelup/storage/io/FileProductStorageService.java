package org.levelup.storage.io;

import org.levelup.storage.domain.Product;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileProductStorageService implements ProductStorageService {

    private final String filepath;

    public FileProductStorageService(String filepath) {
        this.filepath = filepath;
    }

    @Override
    public List<Product> loadProducts() {
        List<Product> products = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(filepath))) {
            String line;
            while ((line = reader.readLine()) != null) {
                if (!line.isEmpty()) {
                    Product product = parseString(line);
                    products.add(product);
                }
            }
        } catch (IOException exc) {
            throw new RuntimeException("Ошибка чтения файла с товарами...", exc);
        }
        return products;
    }

    private Product parseString(String string) {
        String[] fields = string.split(";");

        return new Product(
                fields[0],
                Double.parseDouble(fields[1]),
                fields[2],
                Integer.parseInt(fields[3]),
                Double.parseDouble(fields[4])
        );
    }

    @Override
    public void saveProducts(List<Product> products) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filepath))) {
            for (Product product : products) {
                writer.write(convertToString(product));
            }
        } catch (IOException exc) {
            throw new RuntimeException("Ошибка записи товаров в файл");
        }
    }

    private String convertToString(Product product) {
        return String.join(
                ";",
                product.getName(),
                String.valueOf(product.getDiagonal()),
                product.getCpu(),
                String.valueOf(product.getRam()),
                String.valueOf(product.getWeight())) + "\n";
    }

}
