package org.levelup.storage.io;

import org.levelup.storage.domain.Product;

import java.util.List;

public interface ProductStorageService {

    List<Product> loadProducts();

    void saveProducts(List<Product> products);

}
