package org.levelup.storage.domain;

// design pattern: Builder
public class Product {

    private String name;
    private double diagonal;
    private String cpu;
    private int ram;
    private double weight;

    public Product(String name, double diagonal, String cpu, int ram, double weight) {
        this.name = name;
        this.diagonal = diagonal;
        this.cpu = cpu;
        this.ram = ram;
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getDiagonal() {
        return diagonal;
    }

    public void setDiagonal(double diagonal) {
        this.diagonal = diagonal;
    }

    public String getCpu() {
        return cpu;
    }

    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    public int getRam() {
        return ram;
    }

    public void setRam(int ram) {
        this.ram = ram;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Name='" + name + '\'' + ", diagonal=" + diagonal + ", cpu='" + cpu + '\'' + ", ram=" + ram + ", weight=" + weight;
    }
}
