package org.levelup.storage;

import org.levelup.storage.io.FileProductStorageService;
import org.levelup.storage.io.ProductStorageService;
import org.levelup.storage.menu.Menu;
import org.levelup.storage.service.ProductService;

import java.lang.reflect.Member;

@SuppressWarnings("ALL")
public class StorageApplication {

    private static final String STORAGE_FILE_PATH = "storage.txt";

    public static void main(String[] args) {
        // Склад
        // Данные хранятся в файлах
        // Печать список товаров
        // Добавление товаров

        // Загрузка товаров из файла
        ProductStorageService storageService = new FileProductStorageService(STORAGE_FILE_PATH);

        Menu menu = new Menu();
        menu.startMenu(ProductService.getInstance(storageService), storageService);







    }

}
