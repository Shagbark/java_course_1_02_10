package org.levelup.storage.service;

import org.levelup.storage.domain.Product;
import org.levelup.storage.io.ProductStorageService;

import java.util.List;

// Singleton
public class ProductService {

    private List<Product> products;

    private ProductService() {}

    private static ProductService instance;

    public static ProductService getInstance(ProductStorageService storageService) {
        if (instance == null) {
            instance = new ProductService();
            instance.products = storageService.loadProducts();
        }
        return instance;
    }

    public void addProduct(Product product) {
        products.add(product);
    }

    public List<Product> getProducts() {
        return products;
    }
}
