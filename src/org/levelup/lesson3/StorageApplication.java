package org.levelup.lesson3;
@SuppressWarnings("ALL")
public class StorageApplication {

    public static void main(String[] args) {
        Storage storage = new Storage(100);

        storage.add("Phone", 103.53);

        Product newProduct = new Product();
        newProduct.name = "Laptop";
        newProduct.weight = 2563;
        newProduct.setupCount(3);

        storage.add(newProduct);

        // storage.products[1] = newProduct;
        Product laptop = new Product();
        laptop.name = "Laptop";
        laptop.weight = 2563;
        laptop.setupCount(3);

        Product acer = laptop;

        boolean isEqual = newProduct == laptop;
        // acer == laptop -> true

        acer.setupCount(10);
        boolean isCountEqual = acer.count == laptop.count;


        int a = 4;
        int b = a;

        b = 45;




    }

}
