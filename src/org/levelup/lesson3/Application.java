package org.levelup.lesson3;

import java.util.*;

public class Application {

    public static void main(String[] args) {
        // Склад
        // 1. Внести товар на склад
        //      String[] names; Мышка
        //      double[] weights; 42.34
        System.out.println("Введите название товара");
        Scanner sc = new Scanner(System.in);
        String enteredName = sc.nextLine();

        System.out.println("Введите вес");
        double enteredWeight = sc.nextDouble();

        System.out.println("Введите количество");
        int enteredCount = sc.nextInt();

        // object, объект, ссылка, экземпляр класс, reference, instance
        Product p1;     // null -> NullPointerException (NPE)
        p1 = new Product(); // ссылка на пямять

        p1.name = enteredName;
        p1.count = enteredCount;
        p1.weight = enteredWeight;

        System.out.println(p1.name + " " + p1.weight);
        System.out.println("Текущее количетсво: " + p1.count);
        int newCount = p1.increaseAndGet(10);
        System.out.println("Количество после увеличения: " + newCount);

    }

}
