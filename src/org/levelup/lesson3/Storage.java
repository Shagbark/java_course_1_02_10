package org.levelup.lesson3;


// Сигнатура метода - название метода + параметры (порядок параметров, тип)

// Overloading - перегрузка методов
// add(String s, double d)
// add(String, double, int)
// add(String s1, double d1) - так нельзя
// add(double, String)
// add(double, double)
// add(int, int)
@SuppressWarnings("ALL")
public class Storage {

    // access modifiers (модификаторы доступа)
    // - классов
    // - методов
    // - полей

    // private
    // default-package (private-package)
    // protected
    // public

    // Массив товаров
    private Product[] products;

    // constructor
    public Storage() {
        // [null, null, null, null ... , null]
        products = new Product[10];
    }

    public Storage(int length) {
        products = new Product[length];
    }

    // add(name, weight)
    // addProduct(Product)
    // void
    // add("Phone", 4);
    // Product { name = "Phone", weight = 4}
    // [null ... null]
    // index = 0
    // [ ссылка на Product, null ... null]
    public void add(String name, double weight) {
        Product newProduct = new Product();
        newProduct.name = name;
        newProduct.weight = weight;

        // Product p = new Product();
        // p.name = "New product";
        int index = findIndex();
        if (index != products.length) {
            products[index] = newProduct;
        }
    }

    public void add(Product newProduct) {
        int index = findIndex();
        if (index != products.length) {
            products[index] = newProduct;
        }
    }

    private int findIndex() {
        for (int i = 0; i < products.length; i++) {
            if (products[i] == null) {
                return i;
            }
        }
        return products.length;
    }

}
