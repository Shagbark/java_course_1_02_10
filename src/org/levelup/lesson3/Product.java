package org.levelup.lesson3;

// 1 double, 1 int - 32B
// 1 double, 2 int - 32B
// 1 double, 3 int - 40B
// 1 double, 4 int - 40B
// 1 double, 5 int - 48B
@SuppressWarnings("ALL")
public class Product {

    String name;        // field (поле класса)
    double weight;
    int count;

    // return type - тип возвращаемого значения
    // method name
    // input parameters - входящие параметры
    int increaseAndGet(int value) {
        count = count + value;
        return count;
    }

    public void setupCount(int count) {
        if (count >= 0) {
            this.count = count;
        }
    }

}
