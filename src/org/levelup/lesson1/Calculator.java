package org.levelup.lesson1;

import java.util.Scanner;

public class Calculator {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Введите первое число:");
        int firstVar = sc.nextInt();
        System.out.println("Введите второе число:");
        int secondVar = sc.nextInt();

        int result = firstVar / secondVar;
        System.out.println("Результат = " + result);

    }

}
