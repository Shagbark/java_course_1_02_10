package org.levelup.lesson1;

public class HelloWorld {

    // psvm
    public static void main(String[] args) {
        System.out.println("Hello world!");

        // Объявление переменной
        int a;
        a = 2;

        // Инициализация переменной
        int b = 5;

        int sum = a + b;
        System.out.println(sum);
        System.out.println("Сумма равна " + sum);


        System.out.println(a + b + " = sum"); // 7 = sum
        System.out.println("sum = " + a + b); // sum = 25

        int c = 10;
        // c++; c = c + 1;

        System.out.println(c++); // 10
        // System.out.println(c);
        // c = c + 1;
        System.out.println(++c); // 12
        // c = c + 1;
        // System.out.println(c);

    }

}
