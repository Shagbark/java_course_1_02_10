package org.levelup.lesson2;

import java.util.Random;
import java.util.Scanner;

@SuppressWarnings("ALL")
public class GuessNumber {

    // psvm
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        // Случайное число
        Random radomizer = new Random();
        // nextInt() -> [0, 32)
        // int secretNumber = radomizer.nextInt(5); -> [0, 5)
        // [0, 5)
        // [0, 11)
        // -5
        // [-5, 6)
        int secretNumber = radomizer.nextInt(11) - 5;
//        if (number == secretNumber) {
//            // 5 == 5
//            System.out.println("Вы угадали!");
//        } else {
//            if (number > secretNumber) {
//                System.out.println("Вы ввели число больше, чем загаданное");
//            } else {
//                System.out.println("Вы ввели число меньше, чем загаданное");
//            }
//        }

        // if..else if...
        int number = 0;
        int tryCount = 3;

        do {
            System.out.println("У вас есть " + tryCount + " попытка(и)");
            System.out.println("Введите число:");
            number = sc.nextInt();

            if (number > secretNumber) {
                System.out.println("Вы ввели больше");
            } else if (number < secretNumber) {
                System.out.println("Вы ввели меньше");
            }

            tryCount--;
        } while (number != secretNumber && tryCount != 0);

        if (number == secretNumber) {
            System.out.println("Вы угадали!");
        } else {
            System.out.println("Вы не угадали! У вас закончились попытки");
            System.out.println("Секретное число: " + secretNumber);
        }

//        do {
//
//            if (number > secretNumber) {
//                System.out.println("Вы ввели число больше, чем загаданное");
//            } else {
//                System.out.println("Вы ввели число меньше, чем загаданное");
//            }
//
//        } while (number == secretNumber);
//        System.out.println("Вы угадали!");


//        while (a > b) {
//            a = a - b;
//        }

//        if (number == secretNumber) {
//
//        } else if (number > secretNumber) {
//
//        } else {
//
//        }

    }

}
