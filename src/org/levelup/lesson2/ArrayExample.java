package org.levelup.lesson2;

public class ArrayExample {

    public static void main(String[] args) {
        // array
        int[] integers = new int[7];
        integers[0] = 535;
        integers[2] = 865;
        integers[4] = 54;
        // index - offset

        int value = integers[4];
        System.out.println("Значение из 5 ячейки массива: " + value);
        System.out.println("Длинна массива " + integers.length);

        for (int index = 0; index < integers.length; index++) {
            System.out.println(integers[index]);
        }

//        for (;;) {
//            System.out.println("1");
//            break;
//        }

    }

}
