package org.levelup.lesson5;

@SuppressWarnings("ALL")
public class StringExample {

    public static void main(String[] args) {

        String s1 = "string";
        String s2 = "string";
        String s3 = new String("string");

        System.out.println(s1 == s2);
        System.out.println(s1 == s3);

        String s4 = s2.replace("ng", "ngs");
        System.out.println(s2);
        System.out.println(s4);
    }

}
