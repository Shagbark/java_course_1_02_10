package org.levelup.lesson5.square;

import java.io.Serializable;

public class Rectangle extends Shape implements Drawable, Cloneable, Serializable {

    public Rectangle(int width, int height) {
        // int[] sideSizes = new int[4];
        // sideSizes[0] = width;
        // sideSizes[1] = height;
        // sideSizes[2] = width;
        // sideSizes[3] = height;
        // super(sideSizes);
        super(new int[] { width, height, width, height}  );
    }

    @Override
    public double square() {
        return sideSizes[0] * sideSizes[1];
    }

    @Override
    public void draw() {
        System.out.println("[]");
    }
}
