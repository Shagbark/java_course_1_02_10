package org.levelup.lesson5.square;

public class SquareCalculator {

    public static void main(String[] args) {
        // Shape shape = new Shape();

        // Rectangle rec = new Rectangle();
        // Shape shape = rec;
        Shape shape = new Rectangle(10, 5);
        System.out.println("Square: " + shape.square());

        Drawable drawable = new Rectangle(15, 47);
        drawable.draw();

    }

}
