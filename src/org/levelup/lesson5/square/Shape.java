package org.levelup.lesson5.square;

// abstract class
//  Нельзя вызвать конструктор с оператором new() -> Shape shape = new Shape()
//  Может иметь абстратные методы
public abstract class Shape {

    // [4, 2, 4, 2]
    protected int[] sideSizes;

    public Shape(int[] sideSizes) {
        this.sideSizes = sideSizes;
    }

    public abstract double square();



    public double perimeter() {
        double result = 0;
        for (int i = 0; i < sideSizes.length; i++) {
            result = result + sideSizes[i];
        }
        return result;
    }

}
