package org.levelup.lesson5;

public class HashComparison {

    public static void main(String[] args) {
        Product[] products = new Product[] {
                new Product("", 12345),
                new Product("", 53234),
                new Product("", 57399),
                new Product("", 32426),
                new Product("", 78579)
        };

        Product forComparison = new Product("", 78579);
        for (int i = 0; i < products.length; i++) {
            if (products[i].hashCode() == forComparison.hashCode()) {
                if (products[i].equals(forComparison)) {
                    System.out.println("Элемент под индексом " + i);
                }
            }
        }
    }

}
