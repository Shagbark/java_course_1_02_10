package org.levelup.lesson5;

public class VarArgExample {

    public static int sum(int[] sum) {
        int res = 0;
        for (int i = 0; i < sum.length; i++) {
            res = res + sum[i];
        }
        return res;
    }

    public static int sumIntegers(Integer[] values) {
        int res = 0;
        for (int i = 0; i < values.length; i++) {
            res = res + values[i];
        }
        return res;
    }

    public static int sumIntegersVararg(Integer... values) {
        int res = 0;
        for (int i = 0; i < values.length; i++) {
            res = res + values[i];
        }
        return res;
    }

    public static void main(String[] args) {
        // Integer[] integers = new Integer[10];
        // ...
        sumIntegers(new Integer[10]);
        sumIntegersVararg(1, 2, 4, 5, 6, 7, 8);
    }

}
