package org.levelup.lesson5;

@SuppressWarnings("ALL")
public class StaticTest {

    public static void main(String[] args) {
        StaticTest obj = null;
        obj.printMessage("Message");
    }

    static void printMessage(String message) {
        System.out.println(message);
    }

}
