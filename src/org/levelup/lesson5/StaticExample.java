package org.levelup.lesson5;

public class StaticExample {

    public static void main(String[] args) {
        ClassWithStatic.doSmth();
        System.out.println(ClassWithStatic.staticField);

        ClassWithStatic.staticField = 15;
    }

}
