package org.levelup.lesson5;

// org.levelup.lesson5.Product
public class Product {

    private String name;
    private int vendorCode;

    // private org.levelup.lesson4.Product product;
    // private org.levelup.lesson3.Product product3;

    public Product(String name, int vendorCode) {
        this.name = name;
        this.vendorCode = vendorCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        Product other = (Product) obj;
        return vendorCode == other.vendorCode;
    }

    @Override
    public int hashCode() {
        // Небольшое просто число
//        int result = 31;
//
//        result = result * 31 + count;
//        result = result * 31 + name.hashCode();
//
//        return result;
        return vendorCode ;
    }

}
