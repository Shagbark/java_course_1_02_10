package org.levelup.lesson7;

import java.util.*;
import java.util.stream.*;

public class RemoveDuplicates {

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(1);
        list.add(1);
        list.add(1);
        System.out.println(list);

        for (Integer el : list) {
            if (el < 3) {
                list.remove(el);        // ConcurrentModificationException (CME)
            }
        }

        Set<Integer> uniqueIntegers = new HashSet<>(list);
        System.out.println(uniqueIntegers);

        LinkedList<Integer> linkedInteger = new LinkedList<>(uniqueIntegers);


        List<Integer> filtered = linkedInteger.stream()
                .filter(el -> el > 3)
                .collect(Collectors.toList());


        // for (Integer value : linkedInteger) { sout(value); }
        Iterator<Integer> iterator = linkedInteger.iterator();
        while (iterator.hasNext()) {
            Integer next = iterator.next();
            if (next < 3) {
                iterator.remove();
                System.out.println(next);
            }
        }
    }


}
