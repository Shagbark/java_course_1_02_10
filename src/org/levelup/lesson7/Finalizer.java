package org.levelup.lesson7;

@SuppressWarnings("ALL")
public class Finalizer {

    private Finalizer instance = null;

    @Override
    protected void finalize() throws Throwable {
        instance = this;
    }

}


// Finalizer f = new Finalizer();
// ....
// delete f (finalize()) -> instance = f;