package org.levelup.lesson7;

// 1var -> raw type (Object)
// 2var -> указать тип у generic (GenericChild extends GenericParent<String>)
// 3var -> У child точно такой же тип как у parent (GenericChild<TYPE> extends GenericParent<TYPE>)
// 4var -> GenericChild<TYPE> extends GenericParent<String>
public class GenericChild extends GenericParent<String> {

}