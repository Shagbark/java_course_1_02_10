package org.levelup.lesson7;

import java.util.*;

@SuppressWarnings("ALL")
public class WordCounter {

    public static void main(String[] args) {
        // HashMap<String, Integer> map = new HashMap<>();
        //
//        Map<String, Integer> map = new HashMap<>();
//        map.put("Laptop", 25006);
//        map.put("Phone", 12344);
//        map.put("Mouse", 645);
//        map.put("Monitor", 6544);
//        map.put("Keyboard", 357);
//
//        // map.put("Laptop", 15353);
//
//        Integer value = map.get("Laptop");
//
//        Map<String, Integer> doubleCollectionInitialization = new HashMap<>() {{
//            put("Laptop", 25006);
//            put("Phone", 12344);
//            put("Mouse", 645);
//            put("Monitor", 6544);
//            put("Keyboard", 357);
//        }};

        String text = "Lorem Ipsum - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.";

        String formattedText = text.replace(",", "")
                .replace(".", "")
                .replace(";", "")
                .replace("\"", "")
                .replace("\'", "")
                .replace("?", "")
                .replace("!", "");

        String[] words = formattedText.split(" ");

        Map<String, Integer> wordsMap = new HashMap<>();

        // Collections, Array - foreach
        // for (String word : words)
        // ["a", "b", "c", "d"]
        // 1 iteration: word = "a"
        // 2 iteration: word = "b"
        // 3 iteration: word = "c"
        // 4 iteration: word = "d"

        for (String word : words) {
            Integer wordCount = wordsMap.get(word);
            if (wordCount == null) {
                wordsMap.put(word, 1);
            } else {
                wordsMap.put(word, wordCount + 1);
            }
        }

        Set<String> keys = wordsMap.keySet();
        System.out.println(keys.iterator().getClass());
        for (String key : keys) {
            System.out.println(key + " " + wordsMap.get(key));
        }

//        Set<Map.Entry<String, Integer>> entries = wordsMap.entrySet();
//        Collection<Map.Entry<String, Integer>> topWords = new ArrayList<>();
//
//        for (Map.Entry<String, Integer> entry : entries) {
//
//        }

    }

}
